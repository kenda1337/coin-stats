import {createStore} from 'redux';
import {applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import reducer from './store/reducer'

const w: any = window as any;
const composeEnhancers = w.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(
    applyMiddleware(thunk)
));

export default store;