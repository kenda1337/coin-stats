export const currencies = ["USD", "EUR", "CNY"];
export const defaultCurrency = "USD";

export const FETCH_COINS_BEGIN = "FETCH_COINS_BEGIN";
export const FETCH_COINS_ERROR = "FETCH_COINS_ERROR";
export const FETCH_COINS_SUCCESS = "FETCH_COINS_SUCCESS";
export const CHANGE_CURRENCY = "CHANGE_CURRENCY";

export const BASE_URL = "https://api.coinmarketcap.com/v2/ticker/";
