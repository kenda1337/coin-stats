import * as React from "react";
import {FormEvent} from "react";

import {currencies} from "../../constants";
import "./Settings.css";

interface IProps {
    onChange:(e: FormEvent<EventTarget>) => void
}

const Settings = (props: IProps) => {

    return (
        <div className="form-group">
            <select onChange={props.onChange} className="form-control">
                {
                    currencies.map((currency, index) => (
                        <option key={index}>{currency}</option>
                    ))
                }
            </select>
        </div>
    );
};

export default Settings;