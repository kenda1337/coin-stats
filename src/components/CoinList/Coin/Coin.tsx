import {get} from 'lodash';
import * as React from 'react';
import {Link} from "react-router-dom";

import ICoin from "../../../models/Coin";

interface IProps{
    coinInfo: ICoin,
    selectedCurrency: string
}

const Coin = (props: IProps) => {
    const {id, rank, symbol} = props.coinInfo;
    const {selectedCurrency} = props;
    const price = get(props.coinInfo.quotes[selectedCurrency], 'price');
    const percentChange24h = get(props.coinInfo.quotes[selectedCurrency], 'percent_change_24h');

    return (
        price ?
            <tr>
                <th scope="row">{rank}</th>

                <td><b><Link to={`/details/${id}`}>{symbol}</Link></b></td>
                <td>{price.toFixed(2)} {props.selectedCurrency}</td>
                <td style={{color: (percentChange24h > 0) ? '#008000' : '#ff0000'}}>{percentChange24h}&#37;</td>
            </tr>
            : null
    );
};

export default Coin;