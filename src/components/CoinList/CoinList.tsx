import * as React from 'react';
import {connect} from 'react-redux'
import {RingLoader} from 'react-spinners';

import ICoin from "../../models/Coin";
import IState from "../../models/State";
import {fetchCoins} from "../../store/actions";
import Coin from "./Coin/Coin";
import "./CoinList.css"

interface IProps {
    coins: ICoin[],
    loading: boolean,
    selectedCurrency: string,
    fetchCoins: (currencyName: string) => (dispatch: any) => void
}

class CoinList extends React.Component<IProps> {

    public componentDidMount() {
        this.fetchCoins();
    };

    public fetchCoins = () => {
        this.props.fetchCoins(this.props.selectedCurrency);
    };

    public render() {
        const {coins} = this.props;
        const {selectedCurrency} = this.props;
        const {loading} = this.props;

        return (
            <div className="container">
                <a className="glyphicon glyphicon-refresh btnRefresh" onClick={this.fetchCoins}/>
                <table className="table coinList">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Symbol</th>
                        <th scope="col">Price</th>
                        <th scope="col">Change (24h)</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        !loading ?
                            coins.map(coin => (
                                <Coin key={coin.id} coinInfo={coin} selectedCurrency={selectedCurrency}/>
                            ))
                            :
                            <tr>
                                <td colSpan={4}>
                                    <RingLoader
                                        color={'#123abc'}
                                    />
                                </td>
                            </tr>
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}

// TODO: should implement selectors
const mapStateToProps = (state: IState) => {
    const {coins, error, loading, selectedCurrency} = state;
    return {
        coins,
        error,
        loading,
        selectedCurrency
    }
};

const mapDispatchToProps = {
    fetchCoins
};


export default connect(mapStateToProps, mapDispatchToProps)(CoinList);