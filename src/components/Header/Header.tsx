import * as React from 'react';
import {connect} from "react-redux";

import IState from "../../models/State";
import {changeCurrency, fetchCoins} from "../../store/actions";
import Settings from "../Settings/Settings";

interface IProps {
    selectedCurrency: string,
    changeCurrency: (name: string) => {
        payload: { name: string };
        type: string;
    },
    fetchCoins: (currencyName: string) => (dispatch: any) => void
}

class Header extends React.Component<IProps> {

    public handleOnChange = (e: React.FormEvent<EventTarget>): void => {
        const target = e.target as HTMLInputElement;
        const name = target.value;
        this.props.changeCurrency(name);
        this.props.fetchCoins(name);
    };

    public render() {
        return (
            <nav className="navbar navbar-light bg-light">
                <div className="title nav-item">
                    Cryptocurrency prices
                </div>
                <Settings onChange={this.handleOnChange}/>
            </nav>
        );
    }
}

const mapStateToProps = (state: IState) => {
    const { coins, error, loading, selectedCurrency } = state;
    return {
        coins,
        error,
        loading,
        selectedCurrency
    }
};

const mapDispatchToProps = {
    changeCurrency,
    fetchCoins
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);