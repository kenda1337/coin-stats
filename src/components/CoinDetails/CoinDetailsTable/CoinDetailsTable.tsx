import * as React from 'react';

import ICoin from "../../../models/Coin";

interface IProps {
    coin: ICoin,
    selectedCurrency: string
}

const CoinDetailsTable = (props: IProps) => {
    const {selectedCurrency} = props;
    const {rank, name, symbol, total_supply, circulating_supply} = props.coin;
    const {
        price,
        volume_24h,
        percent_change_1h,
        percent_change_24h,
        percent_change_7d,
        market_cap
    } = props.coin.quotes[selectedCurrency];

    return (
        <div>
            <h1>{name} details</h1>
            <table className="table table-dark">
                <tbody>
                <tr>
                    <th scope="row">Rank</th>
                    <td>{rank}</td>
                </tr>
                <tr>
                    <th scope="row">Name</th>
                    <td>{name}</td>
                </tr>
                <tr>
                    <th scope="row">Symbol</th>
                    <td>{symbol}</td>
                </tr>
                <tr>
                    <th scope="row">Price</th>
                    <td>{price.toFixed(2)}  {selectedCurrency}</td>
                </tr>
                <tr>
                    <th scope="row">24h volume</th>
                    <td>{volume_24h}  {selectedCurrency}</td>
                </tr>
                <tr>
                    <th scope="row">Market cap</th>
                    <td>{market_cap}  {selectedCurrency}</td>
                </tr>
                <tr>
                    <th scope="row">1h change</th>
                    <td>{percent_change_1h}</td>
                </tr>
                <tr>
                    <th scope="row">24h change</th>
                    <td>{percent_change_24h}</td>
                </tr>
                <tr>
                    <th scope="row">7d change</th>
                    <td>{percent_change_7d}</td>
                </tr>
                <tr>
                    <th scope="row">Total supply</th>
                    <td>{total_supply}</td>
                </tr>
                <tr>
                    <th scope="row">Available supply</th>
                    <td>{circulating_supply}</td>
                </tr>
                </tbody>
            </table>
        </div>
    );
};

export default CoinDetailsTable;