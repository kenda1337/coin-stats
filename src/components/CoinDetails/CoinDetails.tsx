import axios from "axios";
import * as React from 'react';
import {connect} from 'react-redux'
import {Link} from "react-router-dom";
import {RingLoader} from 'react-spinners';

import {BASE_URL} from "../../constants";
import IState from "../../models/State";
import CoinDetailsTable from "./CoinDetailsTable/CoinDetailsTable";

interface IProps {
    match: any,
    selectedCurrency: string
}

class CoinDetails extends React.Component<IProps, { selectedCoin: any, loading: boolean }> {

    constructor(props: any) {
        super(props);
        this.state = {
            loading: true,
            selectedCoin: null
        };
    }

    public componentDidMount() {
        this.fetchCoin(this.props.selectedCurrency);
    }

    public componentWillReceiveProps(next: IProps) {
        this.fetchCoin(next.selectedCurrency);
    }

    public fetchCoin = (selectedCurrency: string) => {
        this.setState({loading: true});
        const coinId = this.props.match.params.id;

        axios.get(`${BASE_URL}${coinId}/?convert=${selectedCurrency}`)
            .then((res: any) => {
                this.setState(({
                    loading: false,
                    selectedCoin: res.data.data
                }));
            });
    };

    public handleClick = () => {
        this.fetchCoin(this.props.selectedCurrency);
    }

    public render() {

        if (this.state.loading) {
            return (
                <RingLoader
                    color={'#123abc'}
                />
            );
        }

        const {selectedCurrency} = this.props;
        const {selectedCoin} = this.state;

        return (
            <div>
                <Link to={"/"} className="nav-item">Go back</Link><br/>
                <a className="glyphicon glyphicon-refresh btnRefresh" onClick={this.handleClick}/>
                <CoinDetailsTable coin={selectedCoin} selectedCurrency={selectedCurrency}/>
            </div>
        );

    }
}

const mapStateToProps = (state: IState) => {
    const {selectedCurrency} = state;
    return {
        selectedCurrency
    }
};

export default connect(mapStateToProps)(CoinDetails)
