import * as React from 'react';
import {BrowserRouter, Route } from "react-router-dom";

import './App.css';

import CoinDetail from "./components/CoinDetails/CoinDetails";
import CoinList from "./components/CoinList/CoinList";
import Header from "./components/Header/Header";

class App extends React.Component {
    public render() {
        return (
            <div className="App">
                <BrowserRouter>
                    <div>
                        <Route path="/" component={Header}/>
                        <Route path="/" exact={true} component={CoinList}/>
                        <Route path="/details/:id" exact={true} component={CoinDetail}/>
                    </div>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;