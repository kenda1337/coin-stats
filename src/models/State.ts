import ICoin from "./Coin";

export default interface IState {
    coins: ICoin[],
    counter: number,
    error?: any,
    loading: boolean,
    selectedCurrency: string
}