interface ICoinDetails {
    price: number,
    volume_24h: number,
    market_cap: number,
    percent_change_1h: number,
    percent_change_24h: number,
    percent_change_7d: number
}

export default interface ICoin {
    id: number,
    name: string,
    symbol: string,
    rank: number,
    total_supply: number,
    circulating_supply: number,
    quotes: {
        [currencyName: string]: ICoinDetails
    }
}