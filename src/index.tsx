import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from "react-redux";

import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './store';

const app = (
    <Provider store={store}>
        <App/>
    </Provider>
);

ReactDOM.render(
    app,
    document.getElementById('root') as HTMLElement
);

registerServiceWorker();