import {defaultCurrency} from "../constants";
import {
    CHANGE_CURRENCY,
    FETCH_COINS_BEGIN,
    FETCH_COINS_ERROR,
    FETCH_COINS_SUCCESS
} from '../constants';
import IState from "../models/State";

const initialState:IState = {
    coins: [],
    counter: 0,
    error: null,
    loading: false,
    selectedCurrency: defaultCurrency
};

// TODO: if the reducer grows, it should be split and combined with combineReducer
export default function reducer(state:IState = initialState, action: any) {
    switch(action.type) {

        case CHANGE_CURRENCY:
            return {
                ...state,
                selectedCurrency: action.payload.name
            };

        case FETCH_COINS_BEGIN:
            return {
                ...state,
                error: null,
                loading: true
            };

        case FETCH_COINS_SUCCESS:
            return {
                ...state,
                coins: action.payload.coins,
                loading: false
            };

        case FETCH_COINS_ERROR:
            return {
                ...state,
                coins: [],
                error: action.payload.error,
                loading: false,
            };

        default:
            return state;
    }
}
