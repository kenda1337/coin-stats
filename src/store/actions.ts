import axios from 'axios';
import {
    BASE_URL,
    CHANGE_CURRENCY,
    FETCH_COINS_BEGIN,
    FETCH_COINS_ERROR,
    FETCH_COINS_SUCCESS
} from "../constants";
import ICoin from "../models/Coin";


export const fetchCoinsBegin = () => ({
    type: FETCH_COINS_BEGIN
});

export const changeCurrency = (name: string) => ({
    payload: {name},
    type: CHANGE_CURRENCY
});

export const fetchCoinsError = (error: any) => ({
    payload: {error},
    type: FETCH_COINS_ERROR
});

export const fetchCoinsSuccess = (coins: ICoin[]) => {
    return {
        payload: {coins},
        type: FETCH_COINS_SUCCESS
    };
};

export const fetchCoins = (currencyName: string) => {
    return (dispatch: any) => {
        dispatch(fetchCoinsBegin());
        axios.get(`${BASE_URL}?start=0&limit=100&structure=array&convert=${currencyName}`)
            .then(res => {
                const coins = res.data.data;
                dispatch(fetchCoinsSuccess(coins));
            })
            .catch(error => dispatch(fetchCoinsError(error)));
    };
};
