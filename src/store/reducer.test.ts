import {CHANGE_CURRENCY, defaultCurrency, FETCH_COINS_BEGIN} from "../constants";
import reducer from "./reducer";

describe("reducer", () => {
    it("should return the initial state", () => {
        expect(reducer(undefined, {})).toEqual({
            coins: [],
            counter: 0,
            error: null,
            loading: false,
            selectedCurrency: defaultCurrency
        });
    });

    it("should change the selectedCurrency to CYN", () => {
        expect(reducer({
            coins: [],
            counter: 0,
            error: null,
            loading: false,
            selectedCurrency: defaultCurrency
        }, {
            payload: {name: "CYN"},
            type: CHANGE_CURRENCY
        })).toEqual({
            coins: [],
            counter: 0,
            error: null,
            loading: false,
            selectedCurrency: "CYN"
        });
    });

    it("should change the loading state to true", () => {
        expect(reducer({
            coins: [],
            counter: 0,
            error: null,
            loading: false,
            selectedCurrency: defaultCurrency
        }, {
            type: FETCH_COINS_BEGIN
        })).toEqual({
            coins: [],
            counter: 0,
            error: null,
            loading: true,
            selectedCurrency: defaultCurrency
        });
    });
});
